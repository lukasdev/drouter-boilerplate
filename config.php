<?php
    ob_start();
    session_start();
    date_default_timezone_set("America/Sao_Paulo");

    define('BASE', 'http://localhost/projetos/drouter-boilerplate/');
    define('BASE_ADMIN', BASE.'admin');
    define('HOST', 'localhost');
    define('USER', 'root');
    define('PASS', '');
    define('DB', 'fabyrodrigues');
    define('PORT', '3306');
<?php

	namespace Projeto\Models;
	use Projeto\DataAccess\BancoDeDados;

	abstract class Model 
	{

		protected $table;
		protected $increment = 'id';
		
		protected $data = [];
		protected $selected = [];
		protected $idSelected;

		protected $banco;

		public function __construct(BancoDeDados $banco)
		{
			$this->banco = $banco;
			$this->setTableName();
		}

		public function __set($key, $val) {
			$this->data[$key] = $val;
		}

		protected function setIncrement($field)
		{
			$this->increment = $field;
		}

		protected function setTableName($tableName = null)
		{
			if (is_null($tableName)) {
				$className = get_class($this);
				$exp = explode('\\',$className);
				$className = end($exp);
				$this->table = strtolower($className).'s';
			} else {
				$this->table = $tableName;
			}
		}

		public function create($data = null)
		{
			if (is_null($data)) {
				$data = $this->data;
			}
			$lastId = $this->banco->create($this->table, $data);
			$this->data = [];

			return $lastId;
		}

		public function query($query)
		{
			return $this->banco->query($query);
		}

		public function getAll($where = null, $order = null)
		{
			$registros = $this->banco->read([], $this->table, $where, $order);

			return $registros;
		}

		public function get()
		{
			$retorno = $this->selected;
			$this->selected = [];
			return $retorno;
		}

		public function find($id)
		{
			$registro = $this->banco->read([], $this->table, "`".$this->increment."` = '{$id}'")[0];
			$this->idSelected = $id;
			$this->selected = $registro;

			return $this;
		}

		public function update($data = null, $id = null)
		{
			if (is_null($data)) {
			$data = $this->data;
			}

			if (is_null($id)) {
				$id = $this->idSelected;
			}
			

			$this->data = [];

			return $this->banco->update($this->table, $data, "`".$this->increment."` = '{$id}'");
		}

		public function delete($id)
		{
			return $this->banco->delete($this->table, "`".$this->increment."` = '{$id}'");
		}
	}

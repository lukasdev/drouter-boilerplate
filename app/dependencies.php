<?php
    $container = $app->getContainer();

    $container->banco = $container->shared(function(){
        return new Projeto\DataAccess\BancoMySQL(HOST, USER, PASS, DB, PORT);
    });

    $container->helper = $container->shared(function() use ($container){
        return new Projeto\Helpers\Helper($container->banco);
    });

    $container->messages = $container->shared(function(){
        return new Projeto\Helpers\Messages();
    });

    $container->auth = $container->shared(function() use ($container){
        return new Projeto\Helpers\Authentication($container->banco, $container->helper);
    });
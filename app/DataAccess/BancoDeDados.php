<?php
	namespace Projeto\DataAccess;

	interface BancoDeDados{
		public function open();
		public function create($table, array $data); //int
		public function read(array $campos, $table, $where, $order); //array
		public function update($table, array $data, $where); //bolean
		public function delete($table, $where); //bolean
		public function close();
	}
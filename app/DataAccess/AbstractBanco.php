<?php
	namespace Projeto\DataAccess;
	
	abstract class AbstractBanco{
		protected $_storage = array();

		public function __construct($host, $user, $pass, $db, $port){
			$this->_storage['host'] = $host;
			$this->_storage['user'] = $user;
			$this->_storage['pass'] = $pass;
			$this->_storage['db'] = $db;
			$this->_storage['port'] = $port;
		}
	}
<?php
	namespace Projeto\DataAccess;

	final class BancoMySQL extends AbstractBanco implements BancoDeDados{
		private $dao;
		public $_mode = array();

		public function __construct($host, $user, $pass, $db, $port = '3306'){
			parent::__construct($host, $user, $pass, $db, $port);

			$this->open();
		}

		final public function open(){
			$this->dao = new \PDO('mysql:host='.$this->_storage['host'].';port='.$this->_storage['port'].';dbname='.$this->_storage['db'], $this->_storage['user'], $this->_storage['pass']);
			$this->dao->exec("set names utf8");
		}

		private function dbFieldsPlaceholder($data){
		    $fields = [];
		    $results = [];

		    foreach($data as $field => $value){
		        $fields[] = " {$field} = ? ";
		        $results[] = $value;
		    }

		    return ['fields' => $fields, 'values' => $results];
		}

		final public function create($table, array $data){
			$dataField = $this->dbFieldsPlaceholder($data);
		    $fields = $dataField['fields'];
		    $results = $dataField['values'];

		    $sql = "INSERT INTO {$table} SET ".implode(", ", $fields);

			$stmt = $this->dao->prepare($sql);

			if($stmt->execute($results)){
				$retorno = $this->dao->lastInsertId();
				$stmt = null;
				return $retorno;
			}
			$stmt = null;
			return 0;
		}

		final public function setMode($mode, $data = null){
			$this->_mode['mode'] = $mode;

			if(!is_null($data)){
				$this->_mode['data'] = $data;
			}

			return $this;
		}

		final public function read(array $campos, $table, $where = null, $order = null){
			$strRead = "SELECT ";
			if(count($campos) == 0){
				$strRead .= "* ";
			}else{
				$implode = '`'.implode('`, `', $campos).'`';
				$strRead .= $implode." ";
			}

			$strRead .= "FROM `".$table."` ";

			if(!is_null($where)){
				$strRead .= "WHERE $where ";
			}

			if(!is_null($order)){
				$strRead .= "ORDER BY $order";
			}

			$stmt = $this->dao->prepare($strRead);

			if(empty($this->_mode)){
				$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			}else{
				$stmt->setFetchMode($this->_mode['mode'], $this->_mode['data']);
				$this->_mode = [];
			}
			$stmt->execute();

			$retorno = $stmt->fetchAll();
			$stmt = null;
			return $retorno;
		}

        public function query($query){
            $stmt = $this->dao->prepare($query);
            if(empty($this->_mode)){
                $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            }else{
                $stmt->setFetchMode($this->_mode['mode'], $this->_mode['data']);
                $this->_mode = [];
            }
            $stmt->execute();

            $retorno = $stmt->fetchAll();
            $stmt = null;
            return $retorno;
        }

		final public function update($table, array $data, $where){
			$dataField = $this->dbFieldsPlaceholder($data);
		    $fields = $dataField['fields'];
		    $results = $dataField['values'];
		    
		    $strUpd = "UPDATE `".$table."` SET ".implode(", ", $fields);

		    $strUpd .= " WHERE $where";

		    $stmt = $this->dao->prepare($strUpd);

		    $retorno = ($stmt->execute($results)) ? true : false;
		    $stmt = null;
		    return $retorno;
		}

		final public function delete($table, $where){
			$strDel = "DELETE FROM `".$table."` WHERE $where";
			$stmt = $this->dao->prepare($strDel);

			$retorno = ($stmt->execute()) ? true : false;
			$stmt = null;
			return $retorno;
		}

		final public function close(){
			$this->dao = null;
		}

		public function __destruct(){
			$this->close();
		}

	}
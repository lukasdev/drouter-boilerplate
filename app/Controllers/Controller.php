<?php
    namespace Projeto\Controllers;

    abstract class Controller
    {
        protected $container;

        public function __construct(\DRouter\Container $container)
        {
                
            $this->container = $container;
            $this->render->setHf('header.php','footer.php');
            $rota = $this->router->getMatchedRoute();
            
            $this->render->setAsGlobal([
                'rota' => $rota->getName()
            ]);
        }

        public function __get($key)
        {
            if ($this->container->{$key}) {
                return $this->container->{$key};
            }
        }
    }
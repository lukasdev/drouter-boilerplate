<?php
    namespace Projeto\Helpers;

    class Paginator
    {
        protected $total;
        protected $quantity;
        protected $link;
        protected $getPage;
        protected $pg;
        protected $linkParams = null;

        public function __construct($total, $link, $quantity, $getPage = 'page')
        {
            $this->total = $total;
            $this->link = $link;
            $this->quantity = $quantity;
            $this->getPage = $getPage;
            $this->pg = (isset($_GET[$this->getPage])) ? $_GET[$this->getPage] : 1;
        }

        public function setLinkParams($links){
            $this->linkParams = $links;
        }

        public function offset()
        {
            $page = $this->pg;
            $offset = (--$page) * $this->quantity;
            return $offset;
        }

        public function links($array = false)
        {
            $links = '';
            $linksRetorno = [];

            $paginas = ceil($this->total/$this->quantity);
            if (is_null($this->linkParams)){
                $link = $this->link.'?'.$this->getPage.'=';
            }else{
                $link = $this->link.'?'.$this->linkParams.'&'.$this->getPage.'=';
            }
            
            $maxlinks = 4;
            if ($this->total > $this->quantity) {
                $links .= '<ul class="pagination">';
                $links .= '<li><a href="'.$link.'1">&laquo;</a></li>';
                $linksRetorno[] = [
                    'link' => $link.'1',
                    'text' => "&laquo;"
                ];

                for($i = $this->pg - $maxlinks; $i<= $this->pg-1; $i++){
                    if($i >= 1){
                        $links .= '<li><a href="'.$link.$i.'">'.$i.'</a></li>';
                        $linksRetorno[] = [
                            'link' => $link.$i,
                            'text' => $i
                        ];
                    }
                }
                $links .= '<li class="active">'.$this->pg.'</li>';
                $linksRetorno[] = [
                    'link' => $link.$this->pg,
                    'text' => $this->pg
                ];
                
                for($i = $this->pg+1; $i<=$this->pg+$maxlinks; $i++){
                    if($i <= $paginas){
                        $links .= '<li><a href="'.$link.$i.'">'.$i.'</a></li>';

                        $linksRetorno[] = [
                            'link' => $link.$i,
                            'text' => $i
                        ];
                    }
                }
                $linksRetorno[] = [
                    'link' => $link.$paginas,
                    'text' => "&raquo;"
                ];
                $links .= '<li><a href="'.$link.$paginas.'">&raquo;</a></li>';
                $links .= '</ul>';
            }
            return ($array) ? $linksRetorno : $links;
        }
    }
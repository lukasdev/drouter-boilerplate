<?php
    namespace Projeto\Helpers;
    use Projeto\Helpers\Slug;

    class Helper
    {
        protected $banco;
        protected $slug;

        public function __construct($banco) {
            $this->slug = new Slug();
            $this->banco = $banco;
        }
        
        public function slug($str) {
            return $this->slug->slug($str);
        }

        public function moedaBr($val) {
            return number_format($val,2,',','.');
        }
        
        public function mapArray(array $funcoes, $filtrar){
            $retorno = [];
            if(is_array($filtrar)){
                foreach($filtrar as $i => $valor){
                    foreach($funcoes as $n => $fnc){
                        $retorno[$i] = $fnc($valor);
                    }
                }
            }else{
                $retorno = $filtrar;
                foreach($funcoes as $n => $fnc){
                    $retorno = $fnc($retorno);
                }
            }

            return $retorno;
        }

        public function load($type, $pageName){
            $settings = require(__DIR__.'/../settings.php');

            $load = ($type == 'js') ? $settings['loadjs'] : $settings['loadcss'];

            if ( in_array($pageName, array_keys($load)) ){
                $carregar = $load[$pageName];

                if (!is_array($carregar)) {

                    if($type == 'js')
                        echo '<script type="text/javascript" src="'.BASE.'public/js/'.$carregar.'?time='.time().'"></script>';
                    else
                        echo '<link href="'.BASE.'public/css/'.$carregar.'?time='.time().'" rel="stylesheet" type="text/css" />';
                    

                } else {
                    foreach ($carregar as $i => $file) {
                        if($type == 'js')
                            echo '<script type="text/javascript" src="'.BASE.'public/js/'.$file.'?time='.time().'"></script>';
                        else
                            echo '<link href="'.BASE.'public/css/'.$file.'?time='.time().'" rel="stylesheet" type="text/css" />';
                    }
                }
            }
        }
    }
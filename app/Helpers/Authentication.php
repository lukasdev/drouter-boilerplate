<?php
namespace Projeto\Helpers;

use Projeto\DataAccess\BancoDeDados;
use Projeto\Helpers\Helper;

class Authentication
{
    protected $db;
    public $login = [];
    public $password = [];
    public $data = array();
    protected $helpers;
    public $algo;

    public function __construct(BancoDeDados $banco, Helper $helper)
    {
        $this->db = $banco;
        $this->helpers = $helper;
    }

    public function getLogged($prefix) {
        $data = $this->data[$prefix];
        $idLogged = $this->isLogged($prefix);

        $logged = $this->db->read([], $data['table'], "`{$data['id']}` = '{$idLogged}'")[0];

        return $logged;
    }

    public function isLogged($prefix)
    {
        if (isset($_SESSION['user_logged'.$prefix])) {
            return $_SESSION['user_logged'.$prefix];
        }

        return false;
    }

    public function setData(array $data)
    {
        #$expected = ['table','id', 'login', 'password', 'options'];
        $this->data = $data;
    }

    protected function getData($prefix)
    {
        return $this->data[$prefix];
    }

    public function setLogin($prefix,$login)
    {
        $this->login[$prefix] = $login;
        
        return $this;
    }

    public function getLogin($prefix)
    {
        return $this->login[$prefix];
    }

    public function setPassword($prefix,$password)
    {
        #if (!ctype_alnum ($password)) {
       
        $this->password[$prefix] = $password;

        return $this;
    }

    public function getPassword($prefix)
    {
        return $this->password[$prefix];
    }

    public function generateHash($senha){
        $options = [];
        foreach($this->data as $prefix => $data): 
            $options = $data['options'];
        endforeach;
        $pass = password_hash(
            $senha,
            PASSWORD_DEFAULT,
            $options
        );

        return $pass;
    }
    
    private function needsRehash($prefix,$passHash)
    {
        $data = $this->getData($prefix);

        $currentHashAlgorithm = PASSWORD_DEFAULT;
        $currentHashOptions = $data['options'];
        $passwordNeedsRehash = password_needs_rehash(
            $passHash,
            $currentHashAlgorithm,
            $currentHashOptions
        );

        if ($passwordNeedsRehash === true) {
            //salva novo hash (isso é pseudo-código)
            $pass = password_hash(
                $this->getPassword(),
                $currentHashAlgorithm,
                $currentHashOptions
            );
            $where = sprintf("`%s` = '%d'", $data['id'], $user[$data['id']]);
            $this->db->update($data['table'], [
                $data['password'] => $pass
            ], $where);
        }
    }

    public function auth($prefix)
    {
        $data = $this->getData($prefix);

        if (
            empty($data) || 
            is_null($this->getLogin($prefix)) || 
            is_null($this->getPassword($prefix))
        ) {
            return false;
        } else {
            $where = sprintf("`%s` = '%s'", $data['login'], $this->getLogin($prefix));
            $user = $this->db->read([], $data['table'], $where);

            if (count($user) == 0) {
                return false;
            } else {
                $user = $user[0];
                $pass = $user[$data['password']];

                if (password_verify($this->getPassword($prefix), $pass) === false) {
                   return false;
                } else {
                    $this->needsRehash($prefix,$pass);
                    $_SESSION['user_logged'.$prefix] = $user[$data['id']];

                    return true;
                }
            }
        }
    }


    public function logIn($prefix)
    {
       
       return $this->auth($prefix);
    }

    public function logOut($prefix)
    {
        unset($_SESSION['user_logged'.$prefix]);
        session_destroy();

        if (!$this->isLogged($prefix)) {
            return true;
        }

        return false;
    }
}
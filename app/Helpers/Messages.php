<?php
    namespace Projeto\Helpers;

    class Messages
    {
        protected $previous;
        protected $storageKey = 'projetoMessages';
        protected $storage;

        public function  __construct()
        {
            if (!isset($_SESSION)) {
                throw new \RuntimeException('A session precisa estar estartada!.');
            }

            $this->storage = &$_SESSION;

            if (isset($this->storage[$this->storageKey]) && is_array($this->storage[$this->storageKey])) {
                $this->previous = $this->storage[$this->storageKey];
            }
            $this->storage[$this->storageKey] = [];
        }

        public function setMessage($key, $message)
        {
            if (!isset($this->storage[$this->storageKey][$key])) {
                $this->storage[$this->storageKey][$key] = $message;
            }
        }

        public function getMessage($key)
        {
            if ($this->has($key)) {
                return $this->previous[$key];
            }

            return null;
        }

        public function has($key)
        {
            return isset($this->previous[$key]);
        }
    }
<?php
    namespace Projeto\Helpers\Middlewares;


    use \DRouter\Middlewares\MiddlewareInterface;
    use \DRouter\Http\Response;

    abstract class Middleware implements MiddlewareInterface
    {
        protected $container;

        public function __construct($container)
        {
            $this->container = $container;
        }


        public abstract function handle($request, $response, $next):Response;

        public function __get($key)
        {
            if ($this->container->{$key}) {
                return $this->container->{$key};
            }
        }

        public function __set($key, $val)
        {
            $this->container->{$key} = $val;
        }
    }
<?php
    include 'config.php';
    include 'vendor/autoload.php';

    $app = new DRouter\App();
    $app->render->setViewsFolder(__DIR__.'/public/views/');

    include 'app/dependencies.php';

    $app->render->setAsGlobal([
        'messages' => $container->messages,
        'helper' => $container->helper,
        'base' => BASE
    ]);
    include 'app/routes.php';

    $app->run();